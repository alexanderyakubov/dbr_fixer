#! /bin/bash

mvn package

docker build -t finmars/dbrok_fixer ./

docker run -it --rm -p 8881:8080 finmars/dbrok_fixer

# TO RUN A TEST: 
# cd test
# ./runtest.sh

