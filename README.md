
# Wtrade data broker

### Recent changes (04.03.2020):

* Callback is realized based on ResultUrl param in src/main/resources/application.properties
* test directory includes example of request
* autotest function was added for callback testing -- presently the app calls this function
* Postges caching was realized, but commented out -- need to discuss DB is in separate container or on the host machine

### Drawback:
* src/main/resources/application.properties is put into war file. After changes of the config params, the app chould be rebuild to compose new war file.



