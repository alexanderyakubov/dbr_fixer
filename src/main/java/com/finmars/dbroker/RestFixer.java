package com.finmars.dbroker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;

import com.finmars.dbroker.rest.RestResponse;

import com.finmars.dbroker.wtrad.WtradQuery;
import com.finmars.dbroker.wtrad.WtradStorRepository;
import com.finmars.dbroker.wtrad.WtradProc;


// Reading JSON from http client
// https://openjdk.java.net/groups/net/httpclient/recipes.html

@RestController
//@RequestMapping("/wtrad")
public class RestFixer {
	
	/*@Autowired*/ //DO NOT FORGET TO COMMENT IT IN !!
	private WtradStorRepository wtradStorRepo;

	// https://stackoverflow.com/questions/49670209/can-spring-map-post-parameters-by-a-way-other-than-requestbody
	@RequestMapping(
			value = "/prices",
			method = RequestMethod.POST
			)
	public @ResponseBody DeferredResult<RestResponse> //ResponseEntity<RestResponse>
				prices(@RequestBody RestResponse response)
	{
		DeferredResult<RestResponse> result = new DeferredResult<RestResponse>();
		WtradProc wtradProc = new WtradProc( wtradStorRepo, result, response);

		new Thread( wtradProc ).start();

		// ERROR: return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);

		return result; //ResponseEntity.ok( wtradProc.response );
	} 


	@RequestMapping(
			value = "/autotest",
			method = RequestMethod.POST
			)
	public ResponseEntity<String>
				autotest(@RequestBody String json)
	{
		System.out.println("AUTOTEST : " + json);
		return ResponseEntity.ok( "OK" );
	} 

	// https://www.programcreek.com/java-api-examples/?class=com.fasterxml.jackson.databind.JsonNode&method=fieldNames
	// https://stackoverflow.com/questions/49670209/can-spring-map-post-parameters-by-a-way-other-than-requestbody
	@RequestMapping(
			value = "/test",
			method = RequestMethod.GET
			)
	public ResponseEntity<String>
				test(@RequestParam("tickers") ArrayList<String> tickers, 
						@RequestParam("date") String strDate ) //"yyyy-MM-dd" */
	{
		WtradQuery wtradQuery = new WtradQuery();
		String strRes = wtradQuery.test(tickers, strDate);
		return ResponseEntity.ok(strRes);
	}

}

