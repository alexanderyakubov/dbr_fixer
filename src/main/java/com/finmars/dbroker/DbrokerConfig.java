package com.finmars.dbroker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.io.File;


public class DbrokerConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(DbrokerConfig.class.getName());

	//public static String baseUrl = "api.worldtradingdata.com/api/v1/history"; //"http://localhost:7079";
	//public static String accessKey = "xqXGTuEafuFWVnETvbipaPOolcg3GP9elTUGqlyWzYcfW0r6kbZGIdnTOvnR"; //"API-KEY";
	//public static String resultUrl = "http://localhost:8080/autotest";
	
	public static Properties properties; 

	// https://stackoverflow.com/questions/9259819/how-to-read-values-from-properties-file	
	// https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config
	static {
        properties = new Properties();
        try {
            File file = ResourceUtils.getFile("classpath:application.properties");
            InputStream in = new FileInputStream(file);
            properties.load(in);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

}
