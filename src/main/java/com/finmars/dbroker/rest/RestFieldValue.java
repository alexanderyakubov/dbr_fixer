package com.finmars.dbroker.rest;

import java.util.Date;
import java.util.TimeZone;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;



public class RestFieldValue {
    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }

    public Double value;

    // https://stackoverflow.com/questions/31822877/jackson-jsonformat-set-date-with-one-day-less/45456037#45456037
    //@JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+1") //timezone = JsonFormat.DEFAULT_TIMEZONE
    @JsonFormat(pattern="yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE) 
    public Date date;

    public Boolean fromCache;
    
}
