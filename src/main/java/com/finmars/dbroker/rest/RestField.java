package com.finmars.dbroker.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;


public class RestField {

    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }

	private String code;
	public List<String> parameters;
	public List<RestFieldValue> values;

	public RestField() {
		//this.code = "";
		this.parameters = new ArrayList<String>();
		this.values = new ArrayList<RestFieldValue>();
	}

	//@JsonIgnore
	public enum FieldCodes {open, high, low, close, volume};

	public void setCode(String newCode) {
		this.code = FieldCodes.valueOf(newCode).name();
	}

	public String getCode() {
		return this.code;
	}

}