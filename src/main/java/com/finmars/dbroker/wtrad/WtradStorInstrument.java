package com.finmars.dbroker.wtrad;

/*
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finmars.dbroker.rest.RestField;



@Configuration
@Entity
public class WtradStorInstrument {
    //	https://spring.io/guides/gs/accessing-data-jpa/
    // one_to_many	
    // https://examples.javacodegeeks.com/enterprise-java/spring/data/spring-data-jparepository-example/

    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    //@Size(min=3,max=3)
    @NotEmpty
    private String ticker;

    // https://money.stackexchange.com/questions/2940/how-to-map-stock-ticker-symbols-to-isin-international-securities-identification
    private String isin;

    @OneToMany(mappedBy="instrument", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = WtradStorValue.class)
    private List<WtradStorValue> values;


    @JsonFormat(pattern="yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
    private Date date;

    protected WtradStorInstrument() {}

    public WtradStorInstrument(String ticker, String isin) {
        this.ticker = ticker;
        this.isin = isin;
        this.date = new Date();
    }

    @Override
    public String toString() {
    return String.format(
        "Customer[id=%d, ticker='%s', isin='%s']",
        id, ticker, isin);
    }


     // @return Long return the id
    public Long getId() {
        return id;
    }


     // @return String return the ticker
    public String getTicker() {
        return ticker;
    }

     // @param ticker the ticker to set
    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

     // @return String return the isin
    public String getIsin() {
        return isin;
    }

     // @param isin the isin to set
    public void setIsin(String isin) {
        this.isin = isin;
    }

     // @return Date return the date
    public Date getDate() {
        return date;
    }

}
*/

public class WtradStorInstrument {}

