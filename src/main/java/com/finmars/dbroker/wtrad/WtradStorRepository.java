package com.finmars.dbroker.wtrad;

/*
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface WtradStorRepository extends JpaRepository<WtradStorValue, Long> {

  List<WtradStorValue> findByInstrument(String instrument);

  Optional<WtradStorValue> findById(Long id);
  
  @Query(
  //  "SELECT val FROM WtradStorValue val AMD WtradStorInstrument inst WHERE inst.id=val.instrument_id AND " +
  //     " inst.ticker=(:pTicker) AND val.field=(:pField) AND  val.date >= (:pDateFrom)  AND " +
  //     " val.date <= (:pDateTo) ORDER BY val.date"
    "SELECT val FROM WtradStorValue val WHERE " +
       " val.instrument=(:pInstrument) AND val.field=(:pField) AND  val.date >= (:pDateFrom)  AND " +
       " val.date <= (:pDateTo) ORDER BY val.date"
  )
  List<WtradStorValue> findByTickerAndDateRange(
        //@Param("pTicker") String pTicker, 
        @Param("pInstrument") String pInstrument,
        @Param("pField") String pField, 
		    @Param("pDateFrom") Date pDateFrom,
		    @Param("pDateTo") Date pDateTo
    );

}
*/


public class WtradStorRepository {}
