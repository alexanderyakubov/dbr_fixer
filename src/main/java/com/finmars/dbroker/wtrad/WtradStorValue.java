package com.finmars.dbroker.wtrad;

/*
import java.util.Date;
import java.util.TimeZone;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finmars.dbroker.rest.RestField;

import org.apache.commons.lang3.time.DateUtils;


@Configuration
@Entity
public class WtradStorValue {
    //	https://spring.io/guides/gs/accessing-data-jpa/
    // one_to_many	
    // https://examples.javacodegeeks.com/enterprise-java/spring/data/spring-data-jparepository-example/

    @Autowired
    public void configureJackson(ObjectMapper objectMapper) {
        objectMapper.setTimeZone(TimeZone.getDefault());
    }


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    //@ManyToOne
    //@JoinColumn(name="instrument_id")
    //private WtradStorInstrument instrument;
    private String instrument;

    //@Size(min=3,max=3)
    //@Enumerated(EnumType.STRING)
    //private RestField.FieldCodes field; // "open", "close", etc
    private String field; // "open", "close", etc


    private Double value;

    // https://stackoverflow.com/questions/29076210/convert-a-object-into-json-in-rest-service-by-spring-mvc
    // https://www.baeldung.com/spring-boot-formatting-json-dates
    //@DateTimeFormat(pattern="yyyy-MM-dd")
    // https://stackoverflow.com/questions/46011703/set-current-timezone-to-jsonformat-timezone-value
    @JsonFormat(pattern="yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
    private Date date;

    protected WtradStorValue() {}

    //public WtradStorValue(WtradStorInstrument instrument, RestField.FieldCodes field, Double value, Date date) {
    public WtradStorValue(String instrument, String field, Double value, Date date) {
        this.instrument = instrument;
        this.field = field;
        this.value = value;
        this.date = date;
    }

    @Override
    public String toString() {
    return String.format(
        "id=%d, instrument='%s', field='%s', value='%f']",
        id, instrument, field, value);
    }

    public Long getId() {
        return id;
    }

    // @return WtradStorInstrument return the instrument
    // public WtradStorInstrument getInstrument() {
    public String getInstrument() {
        return instrument;
    }

    // @param instrument the instrument to set
    //public void setInstrument(WtradStorInstrument instrument) {
    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    // @return Double return the value
    public Double getValue() {
        return value;
    }

    // @param value the value to set
    public void setValue(Double value) {
        this.value = value;
    }

    // @return Date return the date
    public Date getDate() {
        return date;
    }

    // @param date the date to set
    public void setDate(Date date) {
        this.date = date;
    }

}
*/
public class WtradStorValue {}
