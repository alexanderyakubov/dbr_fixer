package com.finmars.dbroker.wtrad;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.finmars.dbroker.DbrokerConfig;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

// https://mkyong.com/java/apache-httpclient-examples/
// https://hc.apache.org/httpcomponents-asyncclient-4.1.x/examples.html -- async

// https://www.baeldung.com/jackson-object-mapper-tutorial
// https://stackoverflow.com/questions/2525042/how-to-convert-a-json-string-to-a-mapstring-string-with-jackson-json

// https://howtodoinjava.com/spring-boot2/resttemplate/resttemplate-post-json-example/ -- Spring RestTemplate
// https://stackoverflow.com/questions/7172784/how-do-i-post-json-data-with-curl -- cURL header:"application/json"
public class WtradQuery {

    public JsonNode gRootNode;

    public void getResult(ArrayList<String> tickers, Date date) throws Exception {

        tickers.add("USD");

        String baseUrl = DbrokerConfig.properties.getProperty("com.finmars.dbroker.wtrad.baseUrl");
        if( baseUrl == null ) {
            throw new RuntimeException("No com.finmars.dbroker.wtrad.baseUrl property is found");
        }
        String accessKey = DbrokerConfig.properties.getProperty("com.finmars.dbroker.wtrad.accessKey");
        if( accessKey == null ) {
            throw new RuntimeException("No com.finmars.dbroker.wtrad.accessKey property is found");
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = formatter.format(date); 

        // https://stackoverflow.com/questions/29858248/reading-value-of-nested-key-in-json-with-java-jackson
        String strURL = baseUrl + strDate + "?access_key=" + accessKey;
        strURL = strURL + "&symbols=" + String.join(",", tickers);
        String res = getUrl( strURL );
        parse(res);
    }
    
    // @param uri: does not include prfix "https://"
    private String getUrl(String uri) throws Exception {
        HttpGet request = new HttpGet(uri);

        // add request headers
        //request.addHeader("custom-key", "mkyong");
        //request.addHeader(HttpHeaders.USER_AGENT, "Googlebot");

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {

            // Get HttpResponse Status
            //System.out.println(response.getProtocolVersion());              // HTTP/1.1
            //System.out.println(response.getStatusLine().getStatusCode());   // 200
            //System.out.println(response.getStatusLine().getReasonPhrase()); // OK
            //System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK

            if(response.getStatusLine().getStatusCode()!=200){
                Integer errCode = response.getStatusLine().getStatusCode();
                String errMsg = response.getStatusLine().getReasonPhrase();
                System.out.println( "Response code:" + errCode.toString() );
                System.out.println( errMsg );
                throw new RuntimeException("Return from http reguest to data provider: " + errCode.toString() + "." + errMsg);
            }

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                // return it as a String
                return EntityUtils.toString(entity);
            }

        }
        return "";
    }

    // https://www.journaldev.com/2324/jackson-json-java-parser-api-example-tutorial
    private void parse(String json) throws Exception{
        ObjectMapper mapper = new ObjectMapper(); 

        /*
        TypeReference<HashMap<String,Object>> typeRef 
                = new TypeReference<HashMap<String,Object>>() {};    
        parsedJson = mapper.readValue(json, typeRef);
        */

        //gParsedJson = mapper.readValue(json, gParsedJson.getClass());
        this.gRootNode = mapper.readTree(json);
    }
    
    public String test(ArrayList<String> tickers, String strDate) {

		Date histDate;
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			histDate = formatter.parse(strDate);			
		} catch(Exception e) {
			return e.getMessage();
		}

		try{
			// https://stackoverflow.com/questions/29858248/reading-value-of-nested-key-in-json-with-java-jackson
			// https://www.programcreek.com/java-api-examples/?class=com.fasterxml.jackson.databind.JsonNode&method=fieldNames
            getResult(tickers, histDate);
            
			String strOutput = "Name: " + gRootNode.path("rates").toString() + "\n";

            JsonNode ratesNode = gRootNode.path("rates");
            JsonNode usdNode = ratesNode.path("USD");
            strOutput = strOutput + "USD: " + Double.toString( usdNode.asDouble() ) + "\n\n";
			for( String ticker : tickers ){
				JsonNode rateElem = ratesNode.get(ticker);
                if (rateElem == null) {
                    System.out.println("IMPORTANT: TICKER '" + ticker + "' IS NOT FOUND");
                    continue;
                }

				//System.out.println("Phone No = "+phone.asLong());
				strOutput = strOutput + "\t" + ticker + ": " + rateElem.toString() + "\n";
            }
			return strOutput; 
		} catch (Exception e) {
			//logger.error(e.getMessage());
			return e.getMessage();
		}

    }
}
