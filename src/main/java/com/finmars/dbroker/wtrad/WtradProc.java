package com.finmars.dbroker.wtrad;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.web.context.request.async.DeferredResult;

import com.finmars.dbroker.DbrokerConfig;
import com.finmars.dbroker.rest.RestField;
import com.finmars.dbroker.rest.RestFieldValue;
import com.finmars.dbroker.rest.RestResponse;


public class WtradProc implements Runnable {

	// https://www.baeldung.com/spring-boot-logging
	// https://examples.javacodegeeks.com/core-java/lang/system/out/logging-system-println-results-log-file-example/
	final static Logger LOGGER = LoggerFactory.getLogger(WtradProc.class);

	//@Autowired -- is removed, this is just a link to
	private WtradStorRepository wtradStorRepo;

    public WtradProc(WtradStorRepository localWtradStorRepo,
                     DeferredResult<RestResponse> localResult, 
                     RestResponse localResponse)
    {
        this.wtradStorRepo = localWtradStorRepo;
        this.result = localResult;
        this.response = localResponse;
    }

    public DeferredResult<RestResponse> result;
    public RestResponse response;


    @Override
    public void run() {
        response.errStatus = 0;
        response.errMsg = "";

        try{
            // Config access > http://dolszewski.com/spring/spring-boot-application-properties-file/

            ArrayList<String> tickers = new ArrayList<String>();

            for( int i=0; i<response.data.items.size(); i++) {
                tickers.add(response.data.items.get(i).reference);

                response.data.items.get(i).fields.clear();
                RestField restField = new RestField();
                restField.setCode("close");
                response.data.items.get(i).fields.add(restField);
            }

            Date currDate = response.data.getDate_to();
            while (getDifferenceDays(response.data.getDate_from(), currDate) >= 0) {

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");        

                WtradQuery wtrade = new WtradQuery();
                wtrade.getResult(tickers, currDate);
                currDate = formatter.parse( wtrade.gRootNode.path("date").asText() );			
            
                JsonNode ratesNode = wtrade.gRootNode.path("rates");
                Double usdRate = ratesNode.path("USD").asDouble();
                for( int i=0; i<response.data.items.size(); i++ ) {
                    String ticker = response.data.items.get(i).reference;
                    JsonNode rateElem = ratesNode.get(ticker);
                    if (rateElem == null) {
                        System.out.println("IMPORTANT: TICKER '" + ticker + "' IS NOT FOUND");
                        continue;
                    }
    
                    RestFieldValue restFieldValue = new RestFieldValue();
                    restFieldValue.date = currDate;
                    restFieldValue.value = usdRate / rateElem.asDouble();
                    restFieldValue.fromCache = false;

                    response.data.items.get(i).fields.get(0).values.add(restFieldValue);
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(currDate);
                calendar.add(Calendar.DAY_OF_YEAR, -1);
                currDate = calendar.getTime();
            }    

            this.postResult();

        } catch (Exception e) {
            //logger.error(e.getMessage());
            response.errStatus = 1;
            response.errMsg = e.getMessage();
            result.setErrorResult(response);
            return;
        } 

        result.setResult(response);
    }

    /*
    public Date getStartDate(Date startDate, List<WtradStorValue> values) {
        Date prevDate = response.data.getDate_from();
        for ( WtradStorValue value : values ) {
            int days = getDifferenceDays(prevDate, value.getDate());
            if (days>3) {
                if (getDifferenceDays(prevDate, startDate)>0) {
                    return prevDate;
                }
            }

            Calendar c = Calendar.getInstance();
            c.setTime(value.getDate());
            int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
            // if the current day is Monday then 2 day break is OK
            if ( days>1 &&  dayOfWeek!=Calendar.MONDAY && dayOfWeek!=Calendar.SUNDAY ) {
                if (getDifferenceDays(prevDate, startDate)>0) {
                    return prevDate;
                }
            }
            prevDate = value.getDate();
        }
        if (getDifferenceDays(prevDate, startDate)>0) {
            return prevDate;
        }
        return startDate;
    }
    */


    public int getDifferenceDays(Date d1, Date d2) {
        int daysdiff = 0;
        long diff = d2.getTime() - d1.getTime();
        long diffDays = Math.round(diff / (24 * 60 * 60 * 1000)); //+ 1;
        daysdiff = (int) diffDays;
        return daysdiff;
    }

    private void postResult() throws Exception {
        String resultUrl = DbrokerConfig.properties.getProperty("com.finmars.dbroker.wtrad.resultUrl");
        if( resultUrl==null ){
            throw new RuntimeException("No com.finmars.dbroker.wtrad.resultUrl property is found");
        }

        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(resultUrl);
     
        String json = mapper.writeValueAsString(response);

        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
     
        CloseableHttpResponse response = client.execute(httpPost);
        if (response.getStatusLine().getStatusCode() != 200) {
            client.close();
            throw new RuntimeException("Executing 'Post of the Result': code not 200, The code=" +
                 Integer.toString(response.getStatusLine().getStatusCode()) + " " + response.getStatusLine().getReasonPhrase() );
        }
        client.close();
    }

}
